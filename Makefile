CC = g++

CFLAGS += -Wall -Wextra
CFLAGS += -g
CFLAGS += -Iinclude
CFLAGS += -std=c++0x

LDFLAGS	+= -lSDL2
LDFLAGS += -lGL
LDFLAGS += -lGLEW

SRCDIR = src
OBJDIR = bin
DOCDIR = docs

PROG = mandelbrot-cpp

SRC = $(wildcard $(SRCDIR)/*.cc)
OBJ = $(patsubst $(SRCDIR)/%.cc,$(OBJDIR)/%.o,$(SRC))

.PHONY: all $(PROG) clean memcheck

.SILENT:

all: $(OBJDIR) $(PROG)

doc: $(DOCDIR) Doxyfile
	doxygen

$(OBJDIR):
	mkdir $(OBJDIR)

$(DOCDIR):
	mkdir $(DOCDIR)

$(OBJDIR)/%.o: $(SRCDIR)/%.cc
	@echo " CC $<"
	$(CC) -c -o $@ $< $(CFLAGS)

$(PROG): $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS)
	@echo " LD $@"

clean:
	rm -rf $(OBJDIR) $(DOCDIR) $(PROG)

memcheck: $(PROG)
	valgrind --tool=memcheck --leak-check=full --track-origins=yes --show-reachable=yes --num-callers=20 --track-fds=yes ./$(PROG)
