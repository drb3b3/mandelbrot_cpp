#ifndef WINDOW_H
#define WINDOW_H

#include <iostream> /* console output */
#include <SDL2/SDL.h> /* for windowing in c++ */
#include <GL/glew.h> /* openGL bindings for c++ */

#include "input_handler.h"
#include "dataset.h"

/**
 * a windows class used for rendering openGL stuff into
 */
class window
{
public:
	/**
	 * Creates a new window.
	 * @param data A pointer to our dataset class where we can find most values
	 * we need for initializing our window eg. size or fullscreen state.
	 */
	window(dataset *data);
	
	/**
	 * Destroys a window and frees all used space.
	 */
	~window(void);

	/**
	 * Initializes the SDL2 window and GLEW (C++ openGL binding).
	 * @return true if initialization was successful, false if not.
	 */
	bool init(void);

	/**
	 * swaps the backbuffer and tells the registered input_handler about input events (if any)
	 */
	void update(void);

	/**
	 * clears the windows pane
	 */
	void clear(void);

	/**
	 * checks whether the screen is closed or not
	 * @return true if it's closed, false if not
	 */
	const bool &is_closed(void) const;

	/**
	 * when this function is called the window will close
	 */
	void close(void);

	/**
	 * checks whether the window is fullscreen or not
	 * @return true if it's in fullscreen mode, false if not
	 */
	const bool &is_fullscreen(void) const;

	/**
	 * gets the current width of the window
	 * @return the windows width
	 */
	const size_t &width(void) const;

	/**
	 * gets the current height of the window
	 * @return the windows height
	 */
	const size_t &height(void) const;

	/**
	 * sets the windows input_handler used for input callback
	 * @param handler a pointer to the input_handler to use
	 */
	void set_input_handler(input_handler *handler);

	/**
	 * Takes a screeshot of the current window and
	 * saves it on the disk (format: YYYY-MM-DD_hh-mm-ss).
	 * @param upload Set to true if the screenshot should be uploaded afterwards.
	 * @return The name of the file the screenshot was saved to. An empty string if there was an error.
	 */
	std::string take_screenshot(bool upload);

	/**
	 * Uploads the saved screenshot.
	 * @param filename The filename of the saved screenshot.
	 * @return The returncode of curl.
	 */
	int upload(const std::string &filename);

	/**
	 * Creates a SDL2 renderer,
	 * @return Pointer to the renderer.
	 */
	SDL_Renderer *create_renderer(void);

	/**
	 * Destroy a SDL2 renderer.
	 * @param renderer The renderer to destroy.
	 */
	void destroy_renderer(SDL_Renderer *renderer);
private:
	dataset *m_data;

	/* a pointer to the SDL2 window */
	SDL_Window *m_window;
	/* the openGL context required for rendering to the window */
	SDL_GLContext m_context;

	input_handler *m_input_handler;

	bool m_closed;
	bool m_fullscreen;
	size_t m_width;
	size_t m_height;
};

#endif /* WINDOW_H */
