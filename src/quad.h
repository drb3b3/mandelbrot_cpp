#ifndef QUAD_H
#define QUAD_H

#include <GL/glew.h>

#include "renderable.h"

/**
 * represents a quad that can be rendered using openGL.
 */
class quad : public renderable
{
public:
	/**
	 * Default constructor for a quad.
	 * Only generates the mesh and generates buffers so the quad is bindable.
	 */
	quad(void);

	/**
	 * Default destructor for a quad.
	 * Frees all space and destroys all openGL buffers.
	 */
	~quad(void);
	
	/**
	 * Renders the quad to the screen.
	 */
	void draw(void) override;

private:
	/* the vertex array object used to hold the data together */
	unsigned int m_vao;
	/* contains the actual vertex data */
	unsigned int m_vbo;
	/* an element buffer object containing the index data */
	unsigned int m_ebo;

};


#endif /* QUAD_H */
