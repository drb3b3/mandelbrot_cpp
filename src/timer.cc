#include "timer.h"

timer::timer(void)
{
	/* ctor */
}

timer::~timer(void)
{
	/* dtor */
}

void timer::start(void)
{
	m_start = std::chrono::high_resolution_clock::now();
}

void timer::stop(void)
{
	m_end = std::chrono::high_resolution_clock::now();
}
