#include <unistd.h>

#include "dataset.h"

class opt_parser
{
public:
	opt_parser(dataset *data);
	~opt_parser(void);

	void parse(int argc, char *argv[]);

private:
	dataset *m_data;
};
