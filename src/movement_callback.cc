#include "movement_callback.h"

#define MOVEMENT_SPEED 0.0005f

movement_callback::movement_callback(dataset *data)
{
	/* ctor */
	m_data = data;
}

movement_callback::~movement_callback(void)
{
	/* dtor */
}

void movement_callback::update(keymap *kmap, float dt)
{
	/**
	 * s = v * t;
	 * s is the distance to move.
	 * v is the velocity of the movement.
	 * t is the time (or dt, as passed by as param).
	 */
	if (kmap->is_keydown(SDLK_PLUS))
		m_data->set_maxit(m_data->max_it() + 100);
	if (kmap->is_keydown(SDLK_MINUS))
		m_data->set_maxit(m_data->max_it() - 100);
	if (kmap->is_keydown(SDLK_w))
		m_data->set_zoom(m_data->zoom() * 0.99f);
	if (kmap->is_keydown(SDLK_s))
		m_data->set_zoom(m_data->zoom() * 1.01f);
	if (kmap->is_keydown(SDLK_RIGHT))
		m_data->move_center_x(MOVEMENT_SPEED * dt);
	if (kmap->is_keydown(SDLK_UP))
		m_data->move_center_y(-MOVEMENT_SPEED * dt);
	if (kmap->is_keydown(SDLK_LEFT))
		m_data->move_center_x(-MOVEMENT_SPEED * dt);
	if (kmap->is_keydown(SDLK_DOWN))
		m_data->move_center_y(MOVEMENT_SPEED * dt);
	/*if (keycode == SDLK_PLUS)
	{
		if (m_data->max_it() <= 100)
		{
			m_data->set_maxit(100);
		}
		m_data->set_maxit(m_data->max_it() * 1.1f);
	}
	if (keycode == SDLK_MINUS)
		m_data->set_maxit(m_data->max_it() * 0.9f);
	if (keycode == SDLK_w)
		m_data->set_zoom(m_data->zoom() * 0.9f);
	if (keycode == SDLK_s)
		m_data->set_zoom(m_data->zoom() * 1.1f);
	if (keycode == SDLK_RIGHT)
		m_data->move_center_x(0.005f);
	if (keycode == SDLK_UP)
		m_data->move_center_y(-0.005f);
	if (keycode == SDLK_LEFT)
		m_data->move_center_x(-0.005f);
	if (keycode == SDLK_DOWN)
		m_data->move_center_y(0.005f);*/
}
