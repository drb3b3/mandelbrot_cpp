#include <iostream>
#include <cmath>

#include "mouse_callback.h"

#define ZOOM_SPEED 0.05f

mouse_callback::mouse_callback(dataset *data) :
	m_left_status(0),
	m_remaining_zoom(0.0f),
	m_mwheel_time(0.0f),
	m_data(data)
{
	/* ctor */
}

mouse_callback::~mouse_callback(void)
{
	/* dtor */
}

void mouse_callback::key_down(SDL_Keycode keycode)
{
	m_left_status = keycode == SDL_BUTTON_LEFT ? 1 : 0;
}

void mouse_callback::key_up(SDL_Keycode keycode)
{
	m_left_status = keycode == SDL_BUTTON_LEFT ? 0 : 1;
}

void mouse_callback::mouse_move(int dx, int dy)
{
	if (m_left_status)
	{
		float scale_x;
		float scale_y;
		scale_x = m_data->complex_size().x / m_data->view_dimensions().x;
		scale_y = m_data->complex_size().y / m_data->view_dimensions().y;
		m_data->move_center_x(-1.0f * dx * scale_x);
		m_data->move_center_y(-1.0f * dy * scale_y);
	}
}

void mouse_callback::mwheel_y(int dx)
{
	m_remaining_zoom += dx;
	m_mwheel_time = 0.0f;
}

void mouse_callback::update(keymap *kmap __attribute__((unused)), float dt)
{
	float last_remaining_zoom;
	float d_zoom;
	if (m_remaining_zoom != 0.0f)
	{
		last_remaining_zoom = m_remaining_zoom;
		m_remaining_zoom *= 0.9f;
		d_zoom = (m_remaining_zoom - last_remaining_zoom) * dt * ZOOM_SPEED;
		/* std::cout << "I should zoom in, d_zoom=" << d_zoom << ", dt=" << dt << std::endl; */
		if (d_zoom > 0.0f)
			m_data->set_zoom(m_data->zoom() * (1 + d_zoom));
		else if (d_zoom < 0.0f)
			m_data->set_zoom(m_data->zoom() * (1 + d_zoom));
	}

	if (std::abs(m_remaining_zoom) < 0.01f) m_remaining_zoom = 0.0f;
}
