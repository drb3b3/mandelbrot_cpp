#include "exit_callback.h"

#include <iostream>

exit_callback::exit_callback(window *win)
{
	/* ctor */
	m_win = win;
}

exit_callback::~exit_callback(void)
{
	/* dtor */
}

void exit_callback::key_down(SDL_Keycode keycode)
{
	if (keycode == SDLK_q || keycode == SDLK_ESCAPE)
	{
		m_win->close();
	}
}
