#include "cpu_renderer.h"

cpu_renderer::cpu_renderer(window *win, const dataset *data) :
        m_win(win),
        m_data(data)
{
        /* ctor*/
        m_renderer = m_win->create_renderer();
}

cpu_renderer::~cpu_renderer(void)
{
        /* dtor */
        m_win->destroy_renderer(m_renderer);
}

float map(float val, float min, float max, float newmin, float newmax)
{
        float perc = (val - min) / (max - min);
        float mapped_val = perc * (newmax - newmin) + newmin;
        return mapped_val;
}

int mandelbrot(void)
{
	return 0;
}

void cpu_renderer::draw(void)
{
        int res;
        unsigned x, y;
        for (x = 0; x < m_win->width(); x++) {
                for (y = 0; y < m_win->height(); y++){
                        res = SDL_SetRenderDrawColor(m_renderer, 0, 0, 0, 0xFF);
                        if (res < 0) {
                                std::cerr << "[ERROR][CPU_RENDERER] Couldn't set render color: " << SDL_GetError() << std::endl;
                        }

                        res = SDL_RenderDrawPoint(m_renderer, x, y);
                        if (res < 0) {
                                std::cout << "[ERROR][CPU_RENDERER] Error rendering point: " << SDL_GetError() << std::endl;
                        }
                }
	}
}
