#include <iostream>
#include <stdio.h>
#include <cstdlib>

#include "opt_parser.h"

opt_parser::opt_parser(dataset *data) :
	m_data(data)
{
	/* ctor */
}

opt_parser::~opt_parser(void)
{
	/* dtor */
}

void opt_parser::parse(int argc, char *argv[])
{
	char *itval;
	int c;
	glm::vec2 center = m_data->center();
	float val;
	while ((c = getopt(argc, argv, "fi:x:y:z:")) != -1) {
		switch (c) {
		case 'f':
			m_data->set_fullscreen(true);
			printf("[OPTPARSER][INFO] Okay okay I'm going fullscreen now\n");
			break;
		case 'i':
			itval = optarg;
			val = std::atof(itval);
			if (val == 0) {
				std::cerr << "[OPTPARSER][ERROR] Invalid max iterations passed as argument." << std::endl;
				break;
			}
			printf("[OPTPARSER][INFO] Setting max it to %d.\n", (int) val);
			m_data->set_maxit((int) val);
			break;
		case 'x':
			itval = optarg;
			val = std::atof(itval);
			printf("[OPTPARSER][INFO] Setting x position to %f.\n", val);
			center.x = val;
			m_data->set_center(center);
			break;
		case 'y':
			itval = optarg;
			val = std::atof(itval);
			printf("[OPTPARSER][INFO] Setting y position to %f.\n", val);
			center.y = val;
			m_data->set_center(center);
			break;
		case 'z':
			itval = optarg;
			val = std::atof(itval);
			if (val == 0) {
				std::cerr << "[OPTPARSER][ERROR] Invalid zoom passed as argument." << std::endl;
				break;
			}
			printf("[OPTPARSER][INFO] Setting zoom to %f.\n", val);
			m_data->set_zoom(val);
			break;
		default:
			printf("[OPTPARSER][INFO] Default.\n");
			break;
		}
	}

}
