#ifndef MOUSE_CALLBACK
#define MOUSE_CALLBACK

#include "input_callback.h"
#include "dataset.h"

/**
 * A class that can be registered to an input_handler class and handles input
 * from the user that should change variables in the dataset passed to the constructor.
 */
class mouse_callback : public input_callback
{
public:
	/**
	 * Contructor used for creating new mouse_callback instances.
	 * @param data A pointer to a dataset object to apply changes to.
	 */
	mouse_callback(dataset *data);

	/**
	 * Default destructor for destroying mouse_callbacks.
	 */
	~mouse_callback(void);

	/**
	 * Called when a key has been pressed.
	 * Only handles left mouse down.
	 * @param keycode The keycode of the pressed key.
	 */
	virtual void key_down(SDL_Keycode keycode) override;

	/**
	 * Called when a key has been released.
	 * Only handles left mouse up.
	 * @param keycode The keycode of the released key.
	 */
	virtual void key_up(SDL_Keycode keycode) override;

	/**
	 * If the mouse has been moved, the delta x and y values,
	 * the distance the mouse has moved, this method is called by
	 * the input_handler it is registered to.
	 * @param dx How far the mouse has moved on the x-axis.
	 * @param dy How far the mouse has moved on the y-axis.
	 */
	virtual void mouse_move(int dx, int dy) override;

	/**
	 * Called when the mousewheel has been moved.
	 * @param dy How far it was moved. (positive values for up, negative for down).
	 */
	virtual void mwheel_y(int dy) override;

	/**
	 * Called every frame.
	 * @param kmap A pointer to the current keymap (not used).
	 * @param dt The time passed since the last frame.
	 */
	virtual void update(keymap *kmap, float dt) override;

private:
	int m_left_status; /* 0 if left mouse button is not pressed, 1 if pressed */
	float m_remaining_zoom; /* how far it still should be zoomed in */
	float m_mwheel_time; /* the time since the user scrolled the mousewheel the last time */
	dataset *m_data;
};

#endif /* MOUSE_CALLBACK */
