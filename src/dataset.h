#ifndef DATASET_H
#define DATASET_H

#define NUM_ALGOS 2

#include <glm/glm.hpp>	/* for openGL maths */
#include "shader.h"	/* to access the mandelbrot shader */

/**
 * Defines the available rendering types.
 * RENDER_TYPE_CPU represents a CPU based rendering engine.
 * RENDER_TYPE_GPU represents a GPU based rendering engine.
 **/
enum render_type {
	RENDER_TYPE_CPU	= 0,
	RENDER_TYPE_GPU = 1,
};

/**
 * stores all data needed for rendering the mandelbrot
 */
class dataset
{
public:
	/**
	 * Creates a new dataset object and sets all default values.
	 */
	dataset(void);
	/**
	 * Destroys a dataset and frees unneeded data.
	 */
	~dataset(void);

	/**
	 * Uploads all the data to the shader so the GPU can use them.
	 * @param pshader A pointer to the shader to upload the data to.
	 */
	void upload(shader *pshader);

	/**
	 * Switches through all available algos (mandelbrot/juliaset etc.).
	 * Continues with the first one if the last one was exceeded.
	 */
	void switch_algo(void);

	/**
	 * Getter for the currently set zomm level.
	 * @return Returns a const reference to the level
	 */
	const float &zoom(void) const;

	/**
	 * Sets the current zoom level.
	 * @param zoom The new zoom level.
	 */
	void set_zoom(float zoom);


	/**
	 * Getter for the current red scale for the shader.
	 * @return Returns a constant reference to the current value.
	 */
	const float &red(void) const;

	/**
	 * Setter for the current red scale for the shader.
	 * @param red The new red scale value to be used.
	 */
	void set_red(float red);

	/**
	 * Getter for the current green scale for the shader.
	 * @return Returns a constant reference to the current value.
	 */
	const float &green(void) const;

	/**
	 * Setter for the current green scale for the shader.
	 * @param green The new green scale value to be used.
	 */
	void set_green(float green);

	/**
	 * Getter for the current blue scale for the shader.
	 * @return Returns a constant reference to the current value.
	 */
	const float &blue(void) const;

	/**
	 * Setter for the current blue scale for the shader.
	 * @param blue The new blue scale value to be used.
	 */
	void set_blue(float blue);

	/**
	 * Getter for the max iterations.
	 * @return returns a const reference to the max iterations
	 */
	const int &max_it(void) const;

	/**
	 * Sets the max iterations.
	 * If max_it is less than 1, it is set to 1 automatically.
	 * @param max_it The new maximum iterations
	 */
	void set_maxit(int max_it);

	/**
	 * Getter for z0 (the first z to be used in the mandelbrot function).
	 * @return returns a const reference to z0
	 */
	const glm::vec2 &z0(void) const;

	/**
	 * Setter for z0 (the first z to be used in the mandelbrot function).
	 * @param z0 the new value for z0
	 */
	void set_z0(glm::vec2 z0);

	/**
	 * getter for p (the hochdingens in the mandelbrot function).
	 * @return returns a const reference to p
	 */
	const glm::vec2 &p(void) const;

	/**
	 * Setter for p (the hochdingens in the mandelbrot function).
	 * @return returns a const reference to p
	 */
	void set_p(glm::vec2 p);

	/**
	 * Getter for the center position of the mandelbrot window.
	 * @return a const reference to the center position
	 */
	const glm::vec2 &center(void) const;

	/**
	 * Setter for the center position of the mandelbrot window.
	 * @param center the value to be set
	 */
	void set_center(glm::vec2 center);

	/**
	 * Getter for the window fullscreen state.
	 * @return a const reference to the fullscreen state.
	 */
	const bool &fullscreen(void) const;

	/**
	 * Setter for the fullscreen state of the window.
	 * @param Fullscreen state of the window.
	 */
	void set_fullscreen(bool fullscreen);

	/**
	 * Moves the center of the screen on the x-axis.
	 * @param x The value to move on the x-axis.
	 */
	void move_center_x(float x);

	/**
	 * Moves the center of the screen on the y-axis.
	 * @param y The value to move on the y-axis.
	 */
	void move_center_y(float y);

	/**
	 * Getter for the size of the complex number pane.
	 * @return a const reference to complex_size
	 */
	const glm::vec2 &complex_size(void) const;

	/**
	 * Setter for the size of the complex number pane.
	 * @param complex_size the new complex_size value
	 */
	void set_complex_size(glm::vec2 complex_size);

	/**
	 * Getter for the view dimensions (the window size actually).
	 * @return a const reference to view_dimensions
	 */
	const glm::vec2 &view_dimensions(void) const;

	/**
	 * Setter for the view dimensions (the window size actually).
	 * @param view_dimensions the new void dimensions value
	 */
	void set_view_dimensions(glm::vec2 view_dimensions);

	/**
	 * Getter for the render type.
	 * @return The render type used.
	 **/
	const render_type &get_rendertype(void) const;

	/**
	 * Setter for the render type.
	 * @param type The new render type to use.
	 */
	void set_rendertype(render_type type);

private:
	render_type	m_rendertype;
	bool		m_fullscreen;
	float		m_zoom;
	int		m_max_it;
	unsigned	m_algo;
	glm::vec2	m_z0;
	glm::vec2	m_p;
	glm::vec2	m_center;
	glm::vec2	m_complex_size;
	glm::vec2	m_view_dimensions;

	float		m_red;
	float		m_green;
	float		m_blue;
};

#endif /* DATASET_H */
