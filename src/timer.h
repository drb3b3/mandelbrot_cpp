#include <chrono>


/**
 * A simple high precision timer class.
 */
class timer
{
public:
	/**
	 * Constructs a new timer object.
	 */
	timer(void);
	/**
	 * Destructs a timer object.
	 */
	~timer(void);

	/**
	 * Starts the timer by setting the m_start timepoint to now().
	 */
	void start(void);
	/**
	 * Stops the timer by setting the m_end timepoint to now().
	 */
	void stop(void);
	
	/**
	 * Calculates and returns the elapsed time in microseconds.
	 * @return The elapsed time in microseconds.
	 */
	template <typename T>
	T diff_us(void)
	{
		return std::chrono::duration_cast<std::chrono::microseconds>
			(m_end - m_start).count();
	}
	/**
	 * Calculates and returns the elapsed time in milliseconds.
	 * @return The elapsed time in milliseconds.
	 */
	template <typename T>
	T diff_ms(void)
	{
		return std::chrono::duration_cast<std::chrono::milliseconds>
			(m_end - m_start).count();
	}
	/**
	 * Calculates the elapsed time in seconds.
	 * @return The elapsed time in seconds.
	 */
	template <typename T>
	T diff_s(void)
	{
		return std::chrono::duration_cast<std::chrono::seconds>
			(m_end - m_start).count();
	}

private:
	std::chrono::high_resolution_clock::time_point	m_start;
	std::chrono::high_resolution_clock::time_point	m_end;
};
