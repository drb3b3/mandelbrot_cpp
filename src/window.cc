#include "window.h"

#include <ctime>
#include <sstream>
#include <thread>
#include <iomanip>
#include "timer.h"

window::window(dataset *data) :
	m_data(data),
	m_window(nullptr),
	m_closed(false),
	m_fullscreen(data->fullscreen()),
	m_width(data->view_dimensions().x),
	m_height(data->view_dimensions().y)
{
	/* ctor */
}

window::~window(void)
{
	/* dtor */
	SDL_GL_DeleteContext(m_context);
	SDL_DestroyWindow(m_window);
	SDL_Quit();
}

bool window::init(void)
{
	timer t;
	t.start();
	SDL_Init(SDL_INIT_EVERYTHING);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE,	8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,	8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,	8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE,	8);
	SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE,	32);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,1);

	m_window = SDL_CreateWindow(
			"Mandelbrot Renderer (c) https://github.com/drb3b3/",
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			m_width, m_height,
			SDL_WINDOW_OPENGL);

	m_context = SDL_GL_CreateContext(m_window);

	/* in case fullscreen is enabled, we should inform SDL2 about
	 * this and set the window size apparently. */
	if (m_fullscreen)
	{
		SDL_DisplayMode dm;

		SDL_SetWindowFullscreen(m_window, SDL_WINDOW_FULLSCREEN);
		if (SDL_GetDesktopDisplayMode(0, &dm) < 0)
		{
			std::cerr << "[ERROR] Couldn't query the windows display mode." << std::endl;
			return false;
		}
		m_width = dm.w;
		m_height = dm.h;
		SDL_SetWindowSize(m_window, dm.w, dm.h);
		std::cout << "[INFO] Set windowsize to w/h "
			<< dm.w << "/" << dm.h << "." << std::endl;

		/* tell the dataset about the width/height change or else stuff gets fucked up */
		m_data->set_view_dimensions(glm::vec2(m_width, m_height));

	}

	glewExperimental = GL_TRUE;
	GLenum status = glewInit();
	if (status != GLEW_OK)
	{
		std::cerr << "[ERROR] Couldn't init glew: " << glewGetErrorString(status) << std::endl;
		return false;
	}

	glViewport(0, 0, m_width, m_height);

	
	t.stop();
	std::cout << "[INFO] Initializing the window took " << t.diff_ms<float>() << "ms" << std::endl;
	std::cout << "[WINDOW][INFO] w/h=" << m_width << "/" << m_height << std::endl;

	return true;
}

void window::update(void)
{
	if (m_data->get_rendertype() == RENDER_TYPE_GPU) {
		SDL_GL_SwapWindow(m_window);
	}
	SDL_Event e;
	while (SDL_PollEvent(&e))
	{
		switch (e.type)
		{
			case SDL_QUIT:
				m_closed = true;
				break;
			case SDL_KEYDOWN:
				m_input_handler->key_down(e.key.keysym.sym);
				break;
			case SDL_KEYUP:
				m_input_handler->key_up(e.key.keysym.sym);
				break;
			case SDL_MOUSEMOTION:
				m_input_handler->move_mouse(e.motion.xrel, e.motion.yrel);
				break;
			case SDL_MOUSEBUTTONDOWN:
				m_input_handler->key_down(e.button.button);
				break;
			case SDL_MOUSEBUTTONUP:
				m_input_handler->key_up(e.button.button);
				break;
			case SDL_MOUSEWHEEL:
				m_input_handler->mwheel_y(e.wheel.y);
				break;
			default:
				break;
		}
		
	}

	clear();
}

void window::clear(void)
{
	if (m_data->get_rendertype() == RENDER_TYPE_GPU) {
		glClearColor(0.5, 0.5, 0.9, 1.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}
}

const bool &window::is_closed(void) const
{
	return m_closed;
}

void window::close(void)
{
	m_closed = true;
}

const bool &window::is_fullscreen(void) const
{
	return m_fullscreen;
}

const size_t &window::width(void) const
{
	return m_width;
}

const size_t &window::height(void) const
{
	return m_height;
}

void window::set_input_handler(input_handler *handler)
{
	m_input_handler = handler;
}

std::string window::take_screenshot(bool upload)
{
	std::time_t time = std::time(nullptr);
	std::stringstream ss;
	ss << std::put_time((std::gmtime(&time)), "%Y-%m-%d_%H:%M:%S.bmp"); /* time format stolen from https://en.cppreference.com/w/cpp/io/manip/put_time */

	/* array containing pixel data. 4bytes per pixel */
	unsigned char *pixels = new unsigned char[m_width * m_height * 4];
	if (!pixels)
	{
		std::cerr << "[ERROR] Couldn't allocate memory for the pixel data." << std::endl;
		return "";
	}
	glReadPixels(0, 0, m_width, m_height, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

	SDL_Surface *surf = SDL_CreateRGBSurfaceFrom(pixels, m_width, m_height, 8*4, m_width * 4, 0, 0, 0, 0);
	SDL_SaveBMP(surf, ss.str().c_str());
	std::cout << "[INFO] Saved screenshot as \"" << ss.str() << "\"!" << std::endl;
	SDL_FreeSurface(surf);
	delete pixels;

	if (upload) this->upload(ss.str());

	return ss.str();
}

int window::upload(const std::string &filename)
{
	std::stringstream ss;
	ss << "curl -X POST ";
	ss << "-F \"posx=" << m_data->center().x << "\" ";
	ss << "-F \"posy=" << m_data->center().y << "\" ";
	ss << "-F \"zoom=" << m_data->zoom() << "\" ";
	ss << "-F \"maxit=" << m_data->max_it() << "\" ";
	ss << "-F \"screenshot=@" << filename << "\" ";
	ss << "mandelbrotcpp.eigenanbauer.de/upload.php >> /dev/zero";

	/* std::thread t(std::system(), ss.str().c_str()); */

	return std::system(ss.str().c_str());
}

SDL_Renderer *window::create_renderer(void)
{
	SDL_Renderer *renderer =SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED);
	if (!renderer) {
		std::cerr << "[ERROR] Couldn't create SDL2 renderer." << std::endl;
		return nullptr;
	}
	
	return renderer;
}

void window::destroy_renderer(SDL_Renderer *renderer)
{
	SDL_DestroyRenderer(renderer);
}
