#ifndef MOVEMENT_CALLBACK
#define MOVEMENT_CALLBACK

#include "input_callback.h"
#include "dataset.h"

/**
 * A class that can be registered to an input_handler class and handles input
 * from the user that should change variables in the dataset passed to the constructor.
 * This may be movement (up, down, left, right), in- or decreasing the max. iterations,
 * zooming in and out of the set etc..
 */
class movement_callback : public input_callback
{
public:
	/**
	 * Contructor used for creating new movement_callback instances.
	 * @param data A pointer to a dataset object to apply changes to.
	 */
	movement_callback(dataset *data);

	/**
	 * Default destructor for destroying movement_callbacks.
	 */
	~movement_callback(void);

	/**
	* Called every frame by the input_handler it is registered to.
	* @param kmap A pointer to the keymap of the input_handler this callback is registered to.
	* @param dt The time that passed since the last frame.
	*/
	virtual void update(keymap *kmap, float dt) override;

private:
	dataset *m_data;
};

#endif /* MOVEMENT_CALLBACK */
