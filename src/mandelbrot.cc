#include <iostream> /* for printing to stdout */
#include <SDL2/SDL.h> /* for windowing */
#include <complex> /* support for complex numbers in c++ */
#include <glm/glm.hpp> /* openGL maths functions */

#include "window.h"
#include "timer.h"
#include "shader.h"
#include "quad.h"
#include "cpu_renderer.h"
#include "input_handler.h"
#include "exit_callback.h"
#include "screenshot_callback.h"
#include "algoswitch_callback.h"
#include "movement_callback.h"
#include "mouse_callback.h"
#include "color_callback.h"
#include "dataset.h"
#include "opt_parser.h"

/**
 * Defines the main entry point of the mandelbrot_cpp application.
 * @param argc The count of arguments passed to the program.
 * @param argv An array of arguments passed to the program.
 * @return Returns the exit code of the application. Everything but 0 means an error occured.
 */
int main(int argc, char *argv[])
{
	size_t framec = 0;
	float time = 0;
	timer t;

	dataset data;
	opt_parser parser(&data);
	parser.parse(argc, argv);
	data.set_rendertype(RENDER_TYPE_GPU);
	window win(&data);
	if (!win.init())
	{
		std::cerr << "[ERROR] Window init failed. Quitting. . ." << std::endl;
		return -1;
	}
	movement_callback cb_mvmt(&data);
	mouse_callback cb_mouse(&data);
	color_callback cb_color(&data);
	exit_callback cb_exit(&win);
	screenshot_callback cb_screenshot(&win);
	algoswitch_callback cb_algo(&data);
	input_handler in_handler;
	in_handler.register_callback(&cb_mvmt);
	in_handler.register_callback(&cb_mouse);
	in_handler.register_callback(&cb_exit);
	in_handler.register_callback(&cb_screenshot);
	in_handler.register_callback(&cb_algo);
	in_handler.register_callback(&cb_color);	
	win.set_input_handler(&in_handler);
	t.start();

	/* loading the mandelbrot shader and its settings */
	shader *shaider;
	shaider = new shader("res/shaders/mandelbrot.vs", "res/shaders/mandelbrot.fs");
	shaider->bind();
	

	renderable *renderer;
	if (data.get_rendertype() == RENDER_TYPE_CPU) {
		renderer = new cpu_renderer(&win, &data);
	} else {
		renderer = new quad();
	}

	while (!win.is_closed())
	{
		t.start();
		win.update();

		if (data.get_rendertype() == RENDER_TYPE_GPU)
			data.upload(shaider);

		renderer->draw();
		t.stop();
		in_handler.update(t.diff_ms<float>());
		time += t.diff_ms<float>();
		if (time > 1000)
		{
			std::cout << "[INFO] " << framec << "fps, " << t.diff_ms<float>() << "ms, zoom=" << data.zoom() << ", max_it=" << data.max_it()
			<< ", x/y=" << data.center().x << "/" << data.center().y
			<< " " << data.red() << " red, " << data.green() << " green, " << data.blue() << " blue" << std::endl;
			framec = 0;
			time = 0;
		}
		else
		{
			++framec;
		}
	}

	delete renderer;
	delete shaider;

	return 0;
}

/** \mainpage
 * <center>David Silvan Zingrebe und Tobias Krüger</center>
 * \image html ../logo.bmp
 * \image latex ../logo.bmp
 * \tableofcontents
 * \section vorwort1 Vorwort
 * Das Mandelbrot ist ein luxoriös aussehendes, mathematisches Phänomen, das durch das rekursive Einsetzen einer komplexen Zahl in eine Funktion, welche einen Bildpunkt beschreibt, entsteht. Die dahinter liegende Mathematik ist, im Gegensatz zu der Vielfältigkeit des Mandelbrots, sehr simpel und daher ein sehr beliebtes Informatikprojekt. Zwar mag die tatsächliche Berechnung eines Bildpunkts nicht sehr aufwändig sein, jedoch steigt die Anzahl der Berechnungen mit der steigenden Auflösung der Monitore immer weiter an. Anders als bereits existierende Mandelbrotfraktalrenderer, die entweder ausschließlich die CPU zum Rechnen nutzen, total zugemüllt mit unnützen Funktionen sind oder sich garnicht erst installieren lassen nutzt mandelbrot_cpp die GPU zum Berechnen aus, ist keinen Megabyte groß, kommt mit unter 100MB RAM aus und benötigt lediglich eine langsame CPU. Dazu kommt, daß mandelbrot_cpp freie Software ist. Das heißt: der Quellcode ist frei verfügbar, darf geändert und weiter veröffentlicht werden. (Siehe GPL2 in der LICENSE Datei für weitere Informationen)
 * \section installation Installation
 * \subsection systemrequirements Systemvorraussetzungen
 * <table>
 * <tr><th>CPU</th><td>Toaster</td></tr>
 * <tr><th>RAM</th><td>100MB</td></tr>
 * <tr><th>Disk</th><td>1MB free disk space</td></tr>
 * <tr><th>GPU</th><td>The more the better</td></tr>
 * <tr><th>openGL</th><td>At least version 3.3</td></tr>
 * </table>
 * \subsection installationprocess Anschaffen und Kompilieren des Source Codes
 * mandelbrot_cpp ist ein freier Mandelbrot Renderer. Der Sourcecode kann über git frei bezogen werden.
 * Im Folgenden wird das Programm unter einem *NIX System installiert. Durch die Benutzung von Crossplatform-Bibliotheken ist der Renderer auch unter Windows kompilier- und benutzbar. Die Entwicklungsbibliotheken von SDL2 (für Fenster in c++), glew (openGL bindings für c++), und glm (Mathematische Klassen für openGL) werden zum Kompilieren benötigt. Optional wird doxygen für zum Erstellen dieser Dokumentation benötigt.
 * \code
 * git clone https://github.com/drb3b3/mandelbrot_cpp
 * \endcode
 * Im Rootverzeichnis des Projekts (das Verzeichnis, in dem die Makefile liegt) kann mit dem Befehl \code make \endcode das gesamte Projekt kompiliert werden. Die dadurch entstandende ausführbare Datei kann mit \code ./mandelbrot \endcode ausgeführt werden. Natürlich kann der Renderer auch über einen Dateiexplorer gestartet werden, wobei man jedoch die Konsolenausgaben verliert.
 * \section cmdargs Kommandozeilen Argumente
 * Das Verhalten des Programms kann durch das Anhängen von Kommandozeilenargumenten an den Ausführbefehl verändert werden. Mit einem Fullscreen-Flag kann dafür gesorgt werden, daß das Programm im Vollbildmodus startet.
 * \code
 * ./mandelbrot -f
 * \endcode
 * Die Anzahl der maximalen Iterationen kann mit dem -i Parameter bestimmt werden.
 * \code
 * ./mandelbrot -i 1337
 * \endcode
 * Die Startkoordinaten des Renderers können mit den entsprechenden Befehlen gesetzt werden.
 * \code
 * ./mandelbrot -x 0.69 -y 0.31
 * \endcode
 * Ebenso kann das Zoomlevel bestimmt werden.
 * \code
 * ./mandelbrot -z 31
 * \endcode
 * Diese Argumente können beliebig miteinander kombiniert werden.
 * \section controls Steuerung des Renderers
 * \subsection keybindings Tastaturbelegung
 * Das Programm benutzt die folgende Tastaturbelegung:
 * <table>
 * <tr><th>w</th><td>Hineinzoomen</td></tr>
 * <tr><th>s</th><td>Hinauszoomen</td></tr>
 * <tr><th>Hoch</th><td>Nach oben</td></tr>
 * <tr><th>Runter</th><td>Nach unten</td></tr>
 * <tr><th>Links</th><td>Nach links</td></tr>
 * <tr><th>Rechts</th><td>Nach rechts</td></tr>
 * <tr><th>PRINT oder p</th><td>Screenshot erstellen</td></tr>
 * <tr><th>u</th><td>Screenshot erstellen und hochladen</td></tr>
 * <tr><th>+</th><td>Erhöhen der maximalen Iterationen</td></tr>
 * <tr><th>-</th><td>Vermindern der maximalen Iterationen</td></tr>
 * <tr><th>r</th><td>Aktiviert Rotwert-Veränderungsmodus</td></tr>
 * <tr><th>g</th><td>Aktiviert Grünwert-Veränderungsmodus</td></tr>
 * <tr><th>b</th><td>Aktiviert Blauwert-Veränderungsmodus</td></tr>
 * <tr><th>NUMPAD +</th><td>Erhöht den Farbskalierungswert des derzeitig aktiven Modus</td></tr>
 * <tr><th>NUMPAD -</th><td>Vermindert den Farbskalierungswert des derzeitig aktiven Modus</td></tr>
 * <tr><th>TAB</th><td>Zwischen Mandelbrot und Juliaset wechseln</td></tr>
 * <tr><th>q oder ESC</th><td>Beenden des Programms</td></tr>
 * </table>
 * \subsection moving Bewegung des Sets
 * Um das gesamte Mandelbrot- beziehungsweise Juliaset zu erkunden ist das freie Bewegen in diesen wichtig. Um sich in mandelbrot_cpp durch das Set zu bewegen gibt es zwei verschiedene Möglichkeiten. Zum einen kann, wie in vielen Computerspielen, die Bewegung mit den Pfeiltasten gesteuert werden. Mit PFEILTASTE_HOCH bewegt man sich nach oben, mit PFEILTASTE_RUNTER nach unten. Dementsprechend mit PFEILTASTE_LINKS nach links und mit PFEILTASTE_RECHTS nach rechts. Diese Art der Bewegung hat zwar den Nachteil, daß die Bewegungsgeschwindigkeit konstant ist, ist dafür aber sehr genau. Die andere Bewegungsmöglichkeit ist das Benutzen der Maus. Hierzu drückt man die linke Maustaste an einer beliebigen Stelle im Fenster und zieht lediglich das gesamte Set mit der Maus an die gewünschte Position und lässt die Maustaste wieder los. Diese Art der Bewegung ist ebenfalls sehr präzise, erfordert jedoch eine Computermaus, welche einem unter Umständen nicht immer zur Verfügung steht.
 * \subsection zooming Zoomen des Sets
 * Das Set immer nur aus der Ferne zu betrachten ist langweilig. Deshalb kann man mit den Tasten 'w' und 's' in das Set hinein- und wieder herauszoomen. Die Zoomgeschwindigkeit ist hierbei konstant. Um schneller in das Set zu zoomen, kann das Mausrad benutzt werden. Dreht man dieses nach oben, so wird mit einer sanften Bewegung in das Set hineingezoomt. Mit einem Drehen nach unten kann dementsprechend wieder hinausgezoomt werden. Das derzeitige Zoomlevel kann der Konsolenausgabe entnommen werden.
 * \subsection screenshots Bildschirmaufnahmen
 * Um einen Screenshot des derzeitigen Fensters zu erstellen, kann die Taste 'p' oder PRINT gedrückt werden. Der Screenshot wird dann im Verzeichnis des Projekts mit einem aus Datum und Urhzeit zusammengesetztem Dateinamen gespeicher. Das selbe macht auch die Taste 'u', diese lädt den Screenshot anschließend aber noch auch hoch, sodaß er <a href="http://mandelbrotcpp.eigenanbauer.de/">hier</a> einsehbar ist.
 * \subsection maxit Ändern der maximalen Iterationen
 * Die Anzahl der maximalen Iterationen bestimmt die Genauigkeit des Mandelbrots bzw. des Juliasets. Legt man diese nicht mit einem Kommandozeilenargument selbst fest, so wird der Standardwert von 1000 Iterationen verwendet. Während der Laufzeit des Programms kann mit den Tasten '+' und '-' die Iterationenzahl geändert werden. '+' erhöht die Anzahl um 100 Iterationen während '-' diese wieder um 100 verringert.
 * \subsection colorscale Farbwertskalierung
 * Mit den Tasten 'r', 'g' und 'b' können die Rot-, Grün- und Blaumodi ausgewählt werden. Mit den PLUS- und MINUS-Tasten des Numpads können anschließend die Farbwerte verstärkt oder vermindert werden. Dies dient als kleiner Spezialeffekt um das Aussehen des Sets zu "tunen".
 * \subsection algoswitch Wechseln zwischen Sets
 * Obwohl das Programm "mandelbrot_cpp" heißt, enthält es einen Algorithmus zum Darstellen des Juliasets. Zwischen diesen beiden kann mit der TAB Taste gewechselt werden. Mit jedem Tastendruck wird das jeweils andere Set ausgewäht und im Fenster dargestellt.
 * \subsection quitting Beenden des Programms
 * Um das Programm nach der Benutzung zu schließen kann, falls vorhanden, die 'x' Schaltfläche des Fensters verwendet werden. Da manche Desktopumgebungen diese nicht besitzen und die Schaltfläche im Vollbildmodus ebenfalls vesteckt ist, kann das Programm zusätzlich durch die Tasten 'q' oder 'ESC' beendet werden.
 * \section fordevs Für Entwickler
 * mandelbrot_cpp ist open source software und kann nach der GPL2 Lizens editiert und weiterveröffentlicht werden. Das Programm ist von Grund auf so geschrieben, daß es einfach ist, es zu erweitern. Es bietet aber bereits eine solide Basis als GPU basierter Mandelbrotrenderer. Das Programm ist, um Portabilität, Simplizität und Geschwindigkeit zu gewährleisten, in C++ geschrieben.
 * \subsection pap Grober Programmablauf
 * \image html ../mandelbrot_pap.png
 * \image latex ../mandelbrot_pap.png
 * \subsection gpuinterface Schnittstelle mit der Grafikkarte
 * mandelbrot_cpp benutzt dieselben Bibliotheken wie Spiele um die Grafikkarte anzusprechen. Die Schnittstelle hierfür liefert die crossplatform Bibliothek glew.
 * \subsection gpuutil Utilisieren der Grafikkarte
 * Die "Malfläche", also der gesamte Inhalt des Fensters, besteht aus einem Viereck, welches wiederum aus 2 Dreiecken besteht. Teilt man openGL nun mit, daß man dieses Viereck rendern möchte, durchgehen die Vertexdaten des Vierecks die openGL shader pipeline. Diese besteht in unserem Fall aus einem Vertex Shader, der lediglich die Position der Vertizes and den folgenden Fragment Shader übergibt. Dieser dürchläuft dann für jeden sichtbaren Pixel die Mandelbrot-/Juliaset Funktion und färbt anschließend den Pixel entsprechend der Anzahl der durchlaufenden Iterationen. Dieser Teil des Programms, die Shader, wurden in GLSL (openGL shading language), einer C ähnelnder Sprache, geschrieben, welche auf der Grafikkarte läuft.
 * \subsection thealgo Die Algorithmen
 * \subsubsection algomandelbrot Mandelbrot Algorithmus
 * Der Algorithmus zum Berechnen der benötigten Iterationen im Fragment Shader sieht als Struktogramm wie folgt aus.
 * \image html mandelbrot\_struktogramm.png
 * Hierbei wird zunächst an Hand der Position des Pixels im Fenster die entsprechende Position auf der komplexen Ebene berechnet und in den Variablen x und y gespeichert. In der Variable c wird lediglich die komplexe Zahl mit dem Realanteil x und dem Imaginäranteil y gespeichert. Folgend wird nun rekursiv die Funktion in sich selbst eingesetzt, bis entweder die Anzahl der maximalen Iterationen erreicht ist oder der Punkt außerhalb der Mandelbrotmenge liegt.
 * \subsubsection algojuliaset Juliaset Algorithmus
 * Der Juliaset Algorithmus unterscheidet sich nur wenig von dem Mandelbrot. Die Koordinate des Pixels wird lediglich zu Beginn als z0 eingesetzt und ein konstanter Wert c mit jeder Iteration addiert anstatt die Position des Pixels jedes Mal zu addieren.
 * \image html ../juliaset_struktogramm.png
 * \image latex ../juliaset_struktogramm.png
 * \subsection classdiagram Klassendiagramm
 * C++ ist eine objektorientierte Programmiersprache. Das folgende Klassendiagramm zeigt die Attribute und alle Funktionen aller Klassen sowie die Relationen zwischen Klassen. Genauere Dokumentationen der verschiedenen Klassen und deren Methoden sind im Menüpunkt "Classes" einsehbar.
 * \image html ../classdiagram.png
 * \image latex ../classdiagram.png
 * \subsection extending Erweitern der Applikation
 * Das Erweitern des Programms ist sehr einfach, da beim Entwickeln der Basis auf wiederverwendbaren Code geachtet wurde. Im Folgenden sind mögliche Herangehensweisen an das Erweitern des Programms erklärt.
 * \subsubsection newkeybindings Neue Keybindings
 * - Am einfachsten ist es, eine bereits existierende Callback Klasse zu kopieren, welche von input_callback erbt, und die Klassennamen der alten Klasse durch die neuen zu ersetzen.
 * - Erstellen von Relationen zu anderen Klassen (beispielsweise der dataset Klasse) im Konstruktor.
 * - Geerbte Methoden überschreiben (keydown(SDL_Keycode), keyup(SDL_Keycode), update(dt)).
 * - Implementieren der gewünschten Funktion in den geerbten Methoden (bespielsweise einen Wert der dataset Klasse ändern).
 * - Erstellen einer Instanz des neuen Objekts in der mandelbrot.cc Datei.
 * - Registrieren des Callbacks an den input_handler in der mandelbrot.cc Datei.
 * \subsubsection ownrendering Eigene Sachen zeichnen
 * - Eine Klasse erstellen, die von renderable erbt.
 * - Gegebenfalls Relationen zu anderen Klassen bilden.
 * - Einen SDL_Renderer mit der create_renderer() Funktion der window Klasse erstellen.
 * - Die von renderable geerbte draw() Methode überschreiben und dort mit Hilfe des SDL_Renderer auf das Fenster malen.
 * - In mandelbrot.cc eine Instanz der Klasse erstellen.
 * - In der Programmschleife in mandelbrot.cc die draw() Methode des renderable aufrufen.
 * - Den SDL_Renderer mit der destroy_renderer() Funktion der window Klasse wieder freigeben.
 */

