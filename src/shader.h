#ifndef SHADER_H
#define SHADER_H

#include <glm/glm.hpp>

/**
 * represents a GLSL shader
 */
class shader
{
public:
	/**
	 * constructor for loading and creating a new shader
	 * @param vs_file: path to the vertex shader file to load
	 * @param fs_file: path to the fragment shader file to load
	 */
	shader(const char *vs_file, const char *fs_file);
	
	/**
	 * destroys the shader freeing all the resources it needed
	 */
	~shader(void);

	/**
	 * tells openGL to use this shader for rendering
	 */
	void bind(void);

	/**
	 * set a shaders uniform bool variable
	 * @param location: a string of the variable name the value should be saved to
	 * @param val: the boolean value to save into the uniform variable
	 */
	void set_bool(const char *location, bool val) const;
	
	/**
	 * set a shaders uniform int variable
	 * @param location: a string of the variable name the value should be saved to
	 * @param val: the integer value to save into the uniform variable
	 */
	void set_int(const char *location, int val) const;
	
	/**
	 * set a shaders uniform float variable
	 * @param location: a string of the variable name the value should be saved to
	 * @param val: the float value to save into the uniform variable
	 */
	void set_float(const char *location, float val) const;
	
	/**
	 * set a shaders uniform vec2 variable
	 * @param location: a string of the variable name the value should be saved to
	 * @param val: the vec2 value to save into the uniform variable
	 */
	void set_vec2(const char *location, glm::vec2 val) const;
	
	/**
	 * set a shaders uniform vec3 variable
	 * @param location: a string of the variable name the value should be saved to
	 * @param val: the vec3 value to save into the uniform variable
	 */
	void set_vec3(const char *location, glm::vec3 val) const;
	
	/**
	 * set a shaders uniform vec4 variable
	 * @param location: a string of the variable name the value should be saved to
	 * @param val: the vec4 value to save into the uniform variable
	 */
	void set_vec4(const char *location, glm::vec4 val) const;

private:
	unsigned int m_id; /* the shaders ID it can be accessed with*/
};

#endif /* SHADER_H */
