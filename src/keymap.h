#ifndef KEYMAP_H
#define KEYMAP_H

#include <map>
#include <SDL2/SDL.h>

/**
 * The keymap contains the current state of the keyboard. That are basically just which keys are pressed at the moment. It also contains methods to manipulate the keymap or get the state of a specified key.
 */
class keymap
{
public:
	/**
	 * Created a new keymap object.
	 */
	keymap(void);

	/**
	 * Destroys an existing keymap object.
	 */
	~keymap(void);

	/**
	 * Sets the state of the key keycode to released.
	 * @param keycode The key of which the state should be changed.
	 */
	void keyup(SDL_Keycode keycode);

	/**
	 * Sets the state of the key keycode to pressed.
	 * @param keycode The key of which the state should be changed.
	 */
	void keydown(SDL_Keycode keycode);

	/**
	 * Checks if the key is pressed or not.
	 * @return true if it's pressed, false if not.
	 */
	bool is_keydown(SDL_Keycode keycode) const;

private:
	std::map<SDL_Keycode, bool> *m_keymap;
};

#endif /* KEYMAP_H */
