#ifndef SCREENSHOT_CALLBACK
#define SCREENSHOT_CALLBACK

#include "input_callback.h"
#include "window.h"

/**
 * A class used for checking whether the user wants to exit and exists the app if the user wants to.
 */
class screenshot_callback : public input_callback
{
public:
	/**
	 * Contructor for creating a new instance of the screenshot_callback.
	 * @param win A pointer to the window that should be captured.
	 */
	screenshot_callback(window *win);

	/**
	 * Destructor for destroying useless screenshot_callbacks.
	 */
	~screenshot_callback(void);

	/**
	 * Called when a key is pressed.
	 * PRINT: Creates a screenshot of the current window.
	 * @param keycode The key that was pressed.
	 */
	virtual void key_down(SDL_Keycode keycode) override;

private:
	window	*m_win;
};

#endif /* SCREENSHOT_CALLBACK */
