#include "screenshot_callback.h"

#include <iostream>

screenshot_callback::screenshot_callback(window *win)
{
	/* ctor */
	m_win = win;
}

screenshot_callback::~screenshot_callback(void)
{
	/* dtor */
}

void screenshot_callback::key_down(SDL_Keycode keycode)
{
	if (keycode == SDLK_PRINTSCREEN || keycode == SDLK_p) {
		std::cout << "[INFO] Generating screenshot..." << std::endl;
		m_win->take_screenshot(false);
	} else if (keycode == SDLK_u) {
		std::cout << "[INFO] Generating screenshot..." << std::endl;
		m_win->take_screenshot(true);
	}
}
