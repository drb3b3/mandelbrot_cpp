#ifndef RENDERABLE_H
#define RENDERABLE_H

#include "dataset.h"

class renderable
{
public:
	virtual ~renderable(void) {  };

	virtual void draw(void) = 0;
};

#endif /* RENDERABLE_H */
