#include "quad.h"

#include <iostream>

quad::quad(void)
{
	float vertices[] =
	{
		/* first triangle*/
		1.0f, 1.0f, 0.0f, /* top right*/
		1.0f, -1.0f, 0.0f, /* bottom right */
		-1.0f, -1.0f, 0.0f, /* bottom left */
		-1.0f, 1.0f, 0.0f, /* top left */
	};

	unsigned int indices[] =
	{
		0, 1, 3, /* first triangle */
		1, 2, 3, /* second triangle */
	};

	glGenVertexArrays(1, &m_vao);
	glGenBuffers(1, &m_vbo);
	glGenBuffers(1, &m_ebo);

	glBindVertexArray(m_vao);

	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void *)0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

quad::~quad(void)
{
	glDeleteVertexArrays(1, &m_vao);
	glDeleteBuffers(1, &m_vbo);
	glDeleteBuffers(1, &m_ebo);
}

void quad::draw(void)
{
	glBindVertexArray(m_vao);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

