#include "dataset.h"
#include <iostream>

dataset::dataset()
{
	/* ctor */
	m_rendertype = RENDER_TYPE_CPU;
	m_fullscreen = false;
	m_red = 1.0f;
	m_green = 1.0f;
	m_blue = 1.0f;

	m_zoom = 1.0f;
	m_max_it = 1000;
	m_algo = 0;
	m_z0 = glm::vec2(0.0f, 0.0f);
	m_p = glm::vec2(2.0f, 0.0f);
	m_center = glm::vec2(0.25f, 0.0f);
	m_complex_size = glm::vec2(3.0f, 2.0f);
	m_view_dimensions = glm::vec2(600, 400);
}

dataset::~dataset(void)
{
	/* dtor */
}

void dataset::upload(shader *pshader)
{
	pshader->set_float("zoom", m_zoom);

	pshader->set_float("red", m_red);
	pshader->set_float("green", m_green);
	pshader->set_float("blue", m_blue);

	pshader->set_int("max_it", m_max_it);
	pshader->set_int("algo", m_algo);
	pshader->set_vec2("z0", m_z0);
	pshader->set_vec2("p", m_p);
	pshader->set_vec2("center", m_center);
	pshader->set_vec2("complex_size", m_complex_size);
	pshader->set_vec2("view_dimensions", m_view_dimensions);
}

void dataset::switch_algo(void)
{
	/* m_algo ? m_algo = 0 : m_algo = 1; */
	++m_algo;
	if (m_algo == NUM_ALGOS) m_algo = 0;
	std::cout << "[INFO] algo is now " << m_algo << std::endl;
}

const float &dataset::zoom(void) const
{
	return m_zoom;
}

void dataset::set_zoom(float zoom)
{
	m_zoom = zoom > 0.0f ? zoom : 0;
}

const float &dataset::red(void) const
{
	return m_red;
}

void dataset::set_red(float red)
{
	m_red = red;
}

const float &dataset::green(void) const
{
	return m_green;
}

void dataset::set_green(float green)
{
	m_green = green;
}

const float &dataset::blue(void) const
{
	return m_blue;
}

void dataset::set_blue(float blue)
{
	m_blue = blue;
}

const int &dataset::max_it(void) const
{
	return m_max_it;
}

void dataset::set_maxit(int max_it)
{
	std::cout << "Max it set to " << max_it << std::endl;
	m_max_it = max_it > 1 ? max_it : 1;
}

const glm::vec2 &dataset::z0(void) const
{
	return m_z0;
}

void dataset::set_z0(glm::vec2 z0)
{
	m_z0 = z0;
}

const glm::vec2 &dataset::p(void) const
{
	return m_p;
}

void dataset::set_p(glm::vec2 p)
{
	m_p = p;
}

const glm::vec2 &dataset::center(void) const
{
	return m_center;
}

void dataset::set_center(glm::vec2 center)
{
	m_center = center;
}

const bool &dataset::fullscreen(void) const
{
	return m_fullscreen;
}

void dataset::set_fullscreen(bool fullscreen)
{
	m_fullscreen = fullscreen;
}

void dataset::move_center_x(float x)
{
	m_center.x += x * m_zoom;
}

void dataset::move_center_y(float y)
{
	m_center.y += y * m_zoom;
}

const glm::vec2 &dataset::complex_size(void) const
{
	return m_complex_size;
}

void dataset::set_complex_size(glm::vec2 complex_size)
{
	m_complex_size = complex_size;
}

const glm::vec2 &dataset::view_dimensions(void) const
{
	return m_view_dimensions;
}

void dataset::set_view_dimensions(glm::vec2 view_dimensions)
{
	m_view_dimensions = view_dimensions;
}

const render_type &dataset::get_rendertype(void) const
{
	return m_rendertype;
}

void dataset::set_rendertype(render_type type)
{
	m_rendertype = type;
}
