#ifndef ALGOSWITCH_CALLBACK
#define ALGOSWITCH_CALLBACK

#include "input_callback.h"
#include "dataset.h"

/**
 * A class that can be registered to an input_handler class and handles input
 * from the user that should change the algorythm to use in the shader.
 */
class algoswitch_callback : public input_callback
{
public:
	/**
	 * Contructor used for creating new algoswitch_callback instances.
	 * @param data A pointer to a dataset object to apply changes to.
	 */
	algoswitch_callback(dataset *data);

	/**
	 * Default destructor for destroying algoswitch_callbacks.
	 */
	~algoswitch_callback(void);

	/**
	 * Called when a key is pressed.
	 * Switches to the next algorythm (mandelbrotset/juliaset etc.) when
	 * TAB is pressed.
	 * @param keycode The key that was pressed.
	 */
	virtual void key_down(SDL_Keycode keycode) override;

private:
	dataset *m_data;
};

#endif /* ALGOSWITCH_CALLBACK */
