#include "keymap.h"

keymap::keymap(void)
{
	/* ctor */
	m_keymap = new std::map<SDL_Keycode, bool>;
}

keymap::~keymap(void)
{
	/* dtor */
	delete m_keymap;
}

void keymap::keyup(SDL_Keycode keycode)
{
	/* update the keymap */
	auto it = m_keymap->find(keycode);
	if (it == m_keymap->end())
	{
		/* the key isn't in the map yet, so insert it as not pressed */
		m_keymap->insert(std::pair<SDL_Keycode, bool>(keycode, true));
	}
	else
	{
		/* key is already inserted, so just change its value */
		it->second = false;
	}

	/* or just this method call but it's not supported on all compilers */
	/* m_kbstate.insert_or_assign( keycode, false ); */
}

void keymap::keydown(SDL_Keycode keycode)
{
	auto it = m_keymap->find(keycode);
	if (it == m_keymap->end())
	{
		/* the key isn't in the map yet, so just insert it as pressed */
		m_keymap->insert(std::pair<SDL_Keycode, bool>(keycode, true));
	}
	else
	{
		/* key is already inserted, so just change the value */
		it->second = true;
	}

	/* or just this method call but it's not supported on all compilers */
	/* m_kbstate.insert_or_assign( keycode, true ); */
}

bool keymap::is_keydown(SDL_Keycode keycode) const
{
	auto it = m_keymap->find(keycode);
	if (it == m_keymap->end())
		return false;
	else
		return it->second;
}
