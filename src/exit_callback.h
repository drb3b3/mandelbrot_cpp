#ifndef EXIT_CALLBACK
#define EXIT_CALLBACK

#include "input_callback.h"
#include "window.h"

/**
 * A class used for checking whether the user wants to exit and exists the app if the user wants to.
 */
class exit_callback : public input_callback
{
public:
	/**
	 * Contructor for creating a new instance of the exit_callback.
	 * @param win A pointer to the window that should be closed.
	 */
	exit_callback(window *win);

	/**
	 * Destructor for destroying useless exit_callbacks.
	 */
	~exit_callback(void);

	/**
	 * Called when a key is pressed.
	 * ESCAPE:	exit the application
	 * 'q':		exit the application
	 * @param keycode The key that was pressed.
	 */
	virtual void key_down(SDL_Keycode keycode) override;

private:
	window	*m_win;
};

#endif /* EXIT_CALLBACK */
