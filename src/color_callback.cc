#include <iostream>
#include "color_callback.h"

color_callback::color_callback(dataset *data)
{
	m_data = data;
}

color_callback::~color_callback(void)
{

}

void color_callback::key_down(SDL_Keycode keycode)
{
	//std::cout << "Test msg from color_callback.cc" << std::endl;
	static int colormode = 0;
	
	if (keycode == SDLK_r)
		colormode = 0;
	if (keycode == SDLK_g)
		colormode = 1;
	if (keycode == SDLK_b)
		colormode = 2;

	if (keycode == SDLK_KP_MINUS && colormode == 0)
		m_data->set_red(m_data->red() * 0.9f);
	if (keycode == SDLK_KP_PLUS && colormode == 0)
		m_data->set_red(m_data->red() * 1.1f);

	if (keycode == SDLK_KP_MINUS && colormode == 1)
		m_data->set_green(m_data->green() * 0.9f);
	if (keycode == SDLK_KP_PLUS && colormode == 1)
		m_data->set_green(m_data->green() * 1.1f);

	if (keycode == SDLK_KP_MINUS && colormode == 2)
		m_data->set_blue(m_data->blue() * 0.9f);
	if (keycode == SDLK_KP_PLUS && colormode == 2)
		m_data->set_blue(m_data->blue() * 1.1f);

}
