#include "shader.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <GL/glew.h>
#include <glm/gtc/type_ptr.hpp>

/* loads the contents of a file to dest */
int read_file(const char *filename, std::string *dest)
{
	if (dest != nullptr)
	{
		std::ifstream is(filename, std::ios::binary);
		if(is)
		{
			std::stringstream ss;
			ss << is.rdbuf();
			*dest = ss.str();
		}
		else
		{
			std::cerr << "[ERROR] Couldn't open shader file " << filename << "!" << std::endl;
			return -1;
		}
		is.close();
	}
	else
	{
		std::cerr << "[ERROR] read_file: dest was nullptr" << std::endl;
		return -1;
	}
	return 0;
}

shader::shader(const char *vs_file, const char *fs_file)
{
	/* getting the shaders sources from file */
	int res;
	std::string vs_code_str;
	std::string fs_code_str;

	res = read_file(vs_file, &vs_code_str);
	if (res != 0)
	{
		std::cerr << "[ERROR] Couldn't read vertex shader " << vs_file << std::endl;
	}

	res = read_file(fs_file, &fs_code_str);
	if (res != 0)
	{
		std::cerr << "[ERROR] Couldn't read vertex shader " << fs_file << std::endl;
	}

	/* compiling the actual shaders */
	unsigned int vs, fs;
	int success;
	char infolog[512];

	const char *vs_code = vs_code_str.c_str();
	const char *fs_code = fs_code_str.c_str();

	vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs, 1, &vs_code, nullptr);
	glCompileShader(vs);
	glGetShaderiv(vs, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(vs, 512, nullptr, infolog);
		std::cerr << "[ERROR] Couldn't compile shader " << vs_file << ":" << std::endl
			<< infolog << std::endl;
		std::cerr << vs_code_str << std::endl;
	}

	fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, &fs_code, nullptr);
	glCompileShader(fs);
	glGetShaderiv(fs, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(fs, 512, nullptr, infolog);
		std::cerr << "[ERROR] Couldn't compile shader " << fs_file << ":" << std::endl
			<< infolog << std::endl;
	}

	/* creating the shader program */
	m_id = glCreateProgram();
	glAttachShader(m_id, vs);
	glAttachShader(m_id, fs);
	glLinkProgram(m_id);
	glGetProgramiv(m_id, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(m_id, 512, nullptr, infolog);
		std::cerr << "[ERROR] Couldn't link program:" << std::endl
			<< infolog << std::endl;
	}
	else
	{
		std::cout << "[INFO] Successfully compiled and linked shaders \"" << vs_file << "\" and \"" << fs_file << "\"." << std::endl;
	}

	glDeleteShader(vs);
	glDeleteShader(fs);
}

shader::~shader(void)
{
	glDeleteProgram(m_id);
}

void shader::bind(void)
{
	glUseProgram(m_id);
}

void shader::set_bool(const char *location, bool val) const
{
	glUniform1i(glGetUniformLocation(m_id, location), (int)val);
}

void shader::set_int(const char *location, int val) const
{
	glUniform1i(glGetUniformLocation(m_id, location), (int)val);
}

void shader::set_float(const char *location, float val) const
{
	glUniform1f(glGetUniformLocation(m_id, location), (float)val);
}

void shader::set_vec2(const char *location, glm::vec2 val) const
{
	glUniform2f(glGetUniformLocation(m_id, location), val.x, val.y);
}
void shader::set_vec3(const char *location, glm::vec3 val) const
{
	glUniform3f(glGetUniformLocation(m_id, location), val.x, val.y, val.z);
}
void shader::set_vec4(const char *location, glm::vec4 val) const
{
	glUniform4f(glGetUniformLocation(m_id, location), val.x, val.y, val.z, val.a);
}

