#ifndef COLOR_CALLBACK
#define COLOR_CALLBACK

#include "input_callback.h"
#include "dataset.h"

/**
 * Build on the movement_callback.h class, this class is used to change the color 
 * settings via the '+' and '-' key on the Keypad of ones keyboard.
 */
class color_callback : public input_callback
{
public:
	/**
	 * Contructor used for creating new movement_callback instances.
	 * @param data A pointer to a dataset object to apply changes to.
	 */
	color_callback(dataset *data);

	/**
	 * Default destructor for destroying color_callbacks.
	 */
	~color_callback(void);

	/**
	 * Called when a key is pressed.
	 * r		enable change of the red var / mode 0
	 * g		enable change of the green var / mode 1
	 * b		enable change of the blue var / mode 2
	 * '+':		increase color variable depending on mode
	 * '-':		decrease color variable depending on mode
	 * @param keycode The key that was pressed
	 */
	virtual void key_down(SDL_Keycode keycode) override;
private:
	dataset *m_data;
};

#endif /* COLOR_CALLBACK */
