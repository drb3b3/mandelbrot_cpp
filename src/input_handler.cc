#include <iostream>
#include "input_handler.h"

input_handler::input_handler(void)
{
	/* ctor */
	m_keymap = new keymap();
}

input_handler::~input_handler(void)
{
	/* dtor */
	delete m_keymap;
}

void input_handler::update(float dt)
{
	/* iterate through all registered callbacks and tell them to update */
	std::vector<input_callback *>::iterator cb_it;
	for (cb_it = m_callbacks.begin(); cb_it != m_callbacks.end(); ++cb_it) {
		(*cb_it)->update(m_keymap, dt);
	}
}

void input_handler::key_down(SDL_Keycode keycode)
{
	m_keymap->keydown(keycode);

	/* tell all callbacks about the event */
	std::vector<input_callback *>::iterator cb_it;
	for (cb_it = m_callbacks.begin(); cb_it != m_callbacks.end(); ++cb_it) {
		(*cb_it)->key_down(keycode);
	}
}

void input_handler::key_up(SDL_Keycode keycode)
{
	m_keymap->keyup(keycode);

	/* tell all callbacks about the event */
	std::vector<input_callback *>::iterator cb_it;
	for (cb_it = m_callbacks.begin(); cb_it != m_callbacks.end(); ++cb_it) {
		(*cb_it)->key_up(keycode);
	}
}

void input_handler::move_mouse(int dx, int dy)
{
	/* tell all the callbacks about the event */
	std::vector<input_callback *>::iterator cb_it;
	for (cb_it = m_callbacks.begin(); cb_it != m_callbacks.end(); ++cb_it) {
		(*cb_it)->mouse_move(dx, dy);
	}
}

bool input_handler::is_keydown(SDL_Keycode keycode) const
{
	return m_keymap->is_keydown(keycode);
}

void input_handler::mwheel_y(int dy)
{
	/* tell all the callbacks about the event */
	std::vector<input_callback *>::iterator cb_it;
	for (cb_it = m_callbacks.begin(); cb_it != m_callbacks.end(); ++cb_it) {
		(*cb_it)->mwheel_y(dy);
	}
}

void input_handler::register_callback(input_callback *callback)
{
	m_callbacks.push_back(callback);
}
