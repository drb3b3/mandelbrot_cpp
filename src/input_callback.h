#ifndef INPUT_CALLBACK
#define INPUT_CALLBACK

#include <SDL2/SDL.h>
#include "keymap.h"

/**
 * Can be used to handle input by registering itself as a callback and being called by the input_handler at the right moments
 */
class input_callback
{
public:
	/* Contructor called when creating a new instance of the input_callback */
	input_callback(void) {  };
	/* Destructor called when deleting an instance of the input_callback */
	virtual ~input_callback(void) {  };

	/**
	* Called every frame by the input_handler this callback is registered to.
	* @param kmap A pointer to the input_handlers keymap.
	* @param dt The time that passed since the last frame.
	*/
	virtual void update(keymap *kmap __attribute__((unused)), float dt __attribute__((unused))) {  };

	/**
	 * Called when a key is pressed.
	 * @param keycode The key that was pressed.
	 */
	virtual void key_down(SDL_Keycode keycode __attribute__((unused))) {  };

	/**
	 * Called when a key is released.
	 * @param keycode The key that was released.
	 */
	virtual void key_up(SDL_Keycode keycode __attribute__((unused))) {  };

	/**
	 * Called when the mouse has moved.
	 * @param dx How far the mouse has moved on the x-axis.
	 * @param dy How far the mouse has moved on the y-axis.
	 */
	virtual void mouse_move(int dx __attribute__((unused)), int dy __attribute__((unused))) {  };

	/**
	 * Called when the mousewheel was rotated.
	 * @param dy The distance how foar the mousewheel has been rotated.
	 */
	virtual void mwheel_y(int dy __attribute__((unused))) {  };
};

#endif /* INPUT_CALLBACK */
