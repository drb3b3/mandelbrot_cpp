#ifndef INPUT_HANDLER_H
#define INPUT_HANDLER_H

#include <functional>	/* for passing functions as a callback */
#include <map>		/* for mapping all the keystates */
#include <vector>	/* for just pushing all the callbacks somewhere */
#include <SDL2/SDL.h>	/* for keycodes */
#include "input_callback.h"

/**
 * The input_handler class can save the current input state such as a keyboad map aswell as mouse movement.
 */
class input_handler
{
public:
	/**
	 * Default constructor that creates a new input_handler object.
	 */
	input_handler(void);

	/**
	 * Default destructor that deletes the input_handler object.
	 */
	~input_handler(void);

	/**
	* Updates everything input specific.
	* @param dt The difference in time since the last frame.
	*/
	void update(float dt);

	/**
	 * Sets a keys state to released.
	 * @param keycode: the key which state should be changed to released.
	 */
	void key_up(SDL_Keycode keycode);

	/**
	 * Sets a keys state to pressed.
	 * @param keycode: the key which state should be changed to pressed.
	 */
	void key_down(SDL_Keycode keycode);

	/**
	 * Moves the mouse.
	 * @param dx The distance moved on the x-axis.
	 * @param dy The distance moved on the y-axis.
	 */
	void move_mouse(int dx, int dy);

	/**
	 * Called when the mouse wheel is moved up/down.
	 * @param dy The amount it has moved ap/down (positive for up, negative for down).
	 */
	void mwheel_y(int dy);

	/**
	 * Registers the callback passed by as argument.
	 * @param callback A pointer to the callback to be registered.
	 */
	void register_callback(input_callback *callback);

	/**
	 * Checks whether the key is pressed or not.
	 * @param keycode: the key which state should be checked.
	 * @return true if the key is pressed, false if not.
	 */
	bool is_keydown(SDL_Keycode keycode) const;

private:
	keymap *m_keymap;
	std::vector<input_callback *> m_callbacks;	/* holds all the callbacks */
};

#endif /* INPUT_HANDLER_H */
