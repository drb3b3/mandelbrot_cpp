#ifndef CPU_RENDERER_H
#define CPU_RENDERER_H

#include "renderable.h"
#include "dataset.h"
#include "window.h"

class cpu_renderer : public renderable
{
public:
	cpu_renderer(window *win, const dataset *data);
	~cpu_renderer(void);

	virtual void draw(void) override;

private:
	window *m_win;
	SDL_Renderer *m_renderer;
	const dataset *m_data;
};
#endif /* CPU_RENDERER_H */
