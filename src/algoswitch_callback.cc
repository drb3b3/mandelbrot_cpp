#include "algoswitch_callback.h"

algoswitch_callback::algoswitch_callback(dataset *data) :
	m_data(data)
{
	/* ctor */
}

algoswitch_callback::~algoswitch_callback(void)
{
	/* dtor */
}

void algoswitch_callback::key_down(SDL_Keycode keycode)
{
	if (keycode == SDLK_TAB)
		m_data->switch_algo();
}
