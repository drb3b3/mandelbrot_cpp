#version 330 core 

out vec4 FragColor;


uniform vec2 z0;
uniform vec2 p;
uniform vec2 view_dimensions;
uniform int max_it;
uniform vec2 center;
uniform vec2 complex_size;
uniform float zoom;
uniform int algo;

uniform float red;
uniform float green;
uniform float blue;

float map(float val, float min, float max, float newmin, float newmax)
{
	float perc = (val - min) / (max - min);
	float mapped_val = perc * (newmax - newmin) + newmin;
	return mapped_val;
}

vec2 iPower(vec2 vec, vec2 p)
{
	float r = sqrt( vec.x * vec.x + vec.y * vec.y );
	if ( r == 0.0f )
	{
		return vec2( 0.0f, 0.0f );
	}

	float theta = vec.y != 0 ? atan( vec.y, vec.x ) : ( vec.x < 0 ? 3.14159265f : 0.0f );
	float imt = -p.y * theta;
	float rpowr = pow(r, p.x);
	float angle = p.x * theta + p.y * log(r);
	vec2 powr;
	powr.x = rpowr * exp(imt) * cos(angle);
	powr.y = rpowr * exp(imt) * sin(angle);
	return powr;
}

vec2 i_squared(vec2 vec)
{
	vec2 square;
	square.x = vec.x * vec.x - vec.y * vec.y;
	square.y = 2 * vec.x * vec.y;
	return square;
}

vec4 get_color(int i)
{
	vec4 res;
	if (i == max_it)
	{
		res = vec4(0.0, 0.0, 0.0, 1.0);
	}
	else
	{
		res = vec4(1.0, 1.0, 1.0, 1.0);
	}
	return res;
}

vec4 get_smooth_grayscale_color(int i)
{
	if (i == max_it)
		return vec4(0.0, 0.0, 0.0, 1.0);
	float bright = map(i, 0, max_it, 0, 1);
	return vec4(red*bright,green*bright,blue*bright, 1.0);
}

vec4 get_lsd_color(int i)
{
	if (i == max_it)
		return vec4(0.0, 0.0, 0.0, 1.0);
	float bright = map(i, 0, max_it, 0, 1);
	
	float lsd_red = sin(31 * bright);
	float lsd_green = cos(652 * bright);
	float lsd_blue = tan(64 * bright);
	
	return vec4(lsd_red, lsd_green, lsd_blue, 1.0);
}


int mandelbrot(void)
{
	vec2 ul;
	vec2 lr;
	ul.x = center.x - (complex_size.x * zoom) / 2.0;
	ul.y = center.y - (complex_size.y * zoom) / 2.0;
	lr.x = center.x + (complex_size.x * zoom) / 2.0;
	lr.y = center.y + (complex_size.y * zoom) / 2.0;

	vec2 z = z0;

	float x = ul.x + (lr.x - ul.x) * (gl_FragCoord.x * 1) / (view_dimensions.x - 1);
	float y = lr.y + (ul.y - lr.y) * gl_FragCoord.y / (view_dimensions.y - 1);
	
	vec2 c = vec2(x, y);
	int i = 0;
	while (z.x * z.x + z.y * z.y < 4.0f && i < max_it)
	{
		//z = iPower(z, p) + c;
		z = i_squared(z) + c;
		++i;
	}
	return i;
}

int juliaset(void)
{
	vec2 ul;
	vec2 lr;
	ul.x = center.x - (complex_size.x * zoom) / 2.0;
	ul.y = center.y - (complex_size.y * zoom) / 2.0;
	lr.x = center.x + (complex_size.x * zoom) / 2.0;
	lr.y = center.y + (complex_size.y * zoom) / 2.0;



	float x = ul.x + (lr.x - ul.x) * (gl_FragCoord.x * 1) / (view_dimensions.x - 1);
	float y = lr.y + (ul.y - lr.y) * gl_FragCoord.y / (view_dimensions.y - 1);
	
	vec2 z = vec2(x, y);
	vec2 c = vec2(-0.7269, 0.1889);
	int i = 0;
	while (z.x * z.x + z.y * z.y < 4.0f && i < max_it)
	{
		//z = iPower(z, p) + c;
		z = i_squared(z) + c;
		++i;
	}

	return i;
}

void main(void)
{
	int it;
	if (algo == 0)
		it = mandelbrot();
	else
		it = juliaset();

	//FragColor = get_smooth_grayscale_color(it);
	FragColor = get_lsd_color(it);
}
