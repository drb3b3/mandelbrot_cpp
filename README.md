mandelbrot_cpp
==============
A Mandelbrot-Renderer written in C++ using the graphics card to calculate.

## Usage
Run the program after compilation by typing `./mandelbrot` in the terminal or double-clicking it in your filebrowser of choice. In the terminal you'll see a lot of information about the state the program currently is in, including Framerate, Frametime, zoom-level, position and more. You can move through the mandelbrot with your arrow-keys and zoom in/out with W/S. The amount of maximum iterations can be increased or decreased by pressing and holding '+' or '-'. You can switch between the mandelbrotset and Juliaset using TAB.

## Compilation
* `make` compiles the program itself.
* `make doc` creates the doxygen documentation of the program.
* `make clean` deletes everything that has been compiled (binaries, docs etc.)
Pro tip: Don't event try to compile it on windows. It should be working though since openGL is crossplatform but I wish you good luck at setting up the VS Project and trying to link the libs.

## Dependencies
Since this program heavily relies on the use of the GPU, some libraries are required to compile the program.
* ArchLinux: `# pacman -S glew sdl2 glm`. ArchLinux packages usually contain the development headers by default.
* Debian and co.: `# apt install libglew-dev libsdl2-dev libglm-dev`
* For windows you have to find that out yourself. I'm currently not depressed enough to try that myself.

We use Doxygen for creating our documenation. You need these libraries for it.
* Archlinux: `# pacman -S doxygen ghostscript texlive-most`
* Debian and co.: `apt install doxygen ghostscript texlive`
* On windows I have no clue. Didn't try that yet.
